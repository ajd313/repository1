from sympy import *
import functools
from sympy.plotting import plot
from sympy.matrices import matrix_multiply_elementwise as mme
import numpy as np
from scipy.integrate import solve_ivp # solving differential equations
from scipy import linalg
import matplotlib.pyplot as plt # ploting results
from mpl_toolkits.mplot3d import Axes3D
import doctest
MAT=np.array



#definicja dekoratora log_results


def log_results(func):
    @functools.wraps(func)
    def w_dec(*args, **kwargs):
        res = func(*args, **kwargs)
        t_old = -1 if len(w_dec.t) == 0 else w_dec.t[-1]
        t_new = args[0]
        if t_new > t_old:
            w_dec.log.append(res)
            w_dec.t.append(args[0])
        else:
            f = filter(lambda x: x >= t_new, w_dec.t)
            idx = w_dec.t.index(next(f))
            w_dec.log = w_dec.log[0:idx]+[res]
            w_dec.t = w_dec.t[0:idx]+[t_new]
        return res
    w_dec.log = []
    w_dec.t = []
    return w_dec

#----------------------------------------------------------------------------------------
#||||||||||||||||||        SYMBOLIC VARIABLES            ||||||||||||||||||||||||||||||||
#----------------------------------------------------------------------------------------

# time
t = symbols('t')

# parameters
m, Ix, Iy, Iz               = symbols('m I_x I_y I_z') # mass and interia
I                           = diag(Ix, Iy, Iz)

xpag, ypag, zpag            = symbols('{\\\,}^x{\\!}p^a_g {\\\,}^y{\\!}p^a_g {\\\,}^z{\\!}p^a_g')
xpav, ypav, zpav            = symbols('{\\\,}^x{\\!}p^a_v {\\\,}^y{\\!}p^a_v {\\\,}^z{\\!}p^a_v')
pag                         = Matrix([xpag, ypag, zpag]) # center of mass
pav                         = Matrix([xpav, ypav, zpav]) # center of volume

grav, rho, Vol              = symbols('g \\rho_{air} V') # earth acc., air density, obj volume

# flying robot
x_, y_, z_                  = symbols('x y z', cls=Function)
phi_, tta_, psi_            = symbols('\phi \\theta \psi', cls=Function)
u_, v_, w_, p_, q_, r_      = symbols('u v w p q r', cls=Function)

x, y, z                     = x_(t), y_(t), z_(t)
phi, tta, psi               = phi_(t), tta_(t), psi_(t)
u, v, w, p, q, r            = u_(t), v_(t), w_(t), p_(t), q_(t), r_(t)

nuaa, omaa                    = Matrix([u,v,w]), Matrix([p,q,r])
gamaa, sgamaa               = nuaa.col_join(omaa), Matrix([sign(u),sign(v),sign(w),sign(p),sign(q),sign(r)])
eta                         = Matrix([x,y,z,phi,tta,psi])
Chi                         = Matrix([[phi, tta, psi]]).T
dChi                        = diff(Chi,t)

# s
xs, ys, zs                  = symbols('\o{x} \o{y} \o{z}')
phis, ttas, psis            = symbols('\o{\phi} \o{\\theta} \o{\psi}')
us, vs, ws, ps, qs, rs      = symbols('\o{u} \o{v} \o{w} \o{p} \o{q} \o{r}', constant=True)
gamaas                      = Matrix([us,vs,ws,ps,qs,rs])

# d
xd, yd, zd                  = symbols('x_d y_d z_d')
phid, ttad, psid            = symbols('\phi_d \\theta_d \psi_d')

# aerodynamical coeffiecients
uCd, vCd, wCd, pCd, qCd, rCd            = symbols('{\\\,}^u{\\!}C_d {\\\,}^v{\\!}C_d {\\\,}^w{\\!}C_d {\\\,}^p{\\!}C_d {\\\,}^q{\\!}C_d {\\\,}^r{\\!}C_d')
uCl, vCl, pCl, qCl, rCl                 = symbols('{\\\,}^u{\\!}C_l {\\\,}^v{\\!}C_l {\\\,}^p{\\!}C_l {\\\,}^q{\\!}C_l {\\\,}^r{\\!}C_l')

# tau
tauu_,tauv_,tauw_,taup_,tauq_,taur_     = symbols('\\tau_u \\tau_v \\tau_w \\tau_p \\tau_q \\tau_r', cls=Function)
tauu,tauv,tauw,taup,tauq,taur           = tauu_(t),tauv_(t),tauw_(t),taup_(t),tauq_(t),taur_(t)
tau                                     = Matrix([tauu,tauv,tauw,taup,tauq,taur])

tauus,tauvs,tauws,taups,tauqs,taurs     = symbols('\overline{\\tau}_u \overline{\\tau}_v \overline{\\tau}_w \overline{\\tau}_p \overline{\\tau}_q \overline{\\tau}_r', cls=Function)
taus                                    = Matrix([tauus,tauvs,tauws,taups,tauqs,taurs])

# engines
cN1, cN2, cN3, cN4, cN      = symbols('c_{N1} c_{N2} c_{N3} c_{N4} c_{N}')
cF1, cF2, cF3, cF4, cF      = symbols('c_{F1} c_{F2} c_{F3} c_{F4} c_{F}')
om1_, om2_, om3_, om4_      = symbols('\omega_1 \omega_2 \omega_3 \omega_4',cls=Function)
om1, om2, om3, om4          = om1_(t), om2_(t), om3_(t), om4_(t)
xpgE1, ypgE1, zpgE1         = symbols('{\\\,}^x{\\!}p^g_{E1} {\\\,}^y{\\!}p^g_{E1} {\\\,}^z{\\!}p^g_{E1}')
xpgE2, ypgE2, zpgE2         = symbols('{\\\,}^x{\\!}p^g_{E2} {\\\,}^y{\\!}p^g_{E2} {\\\,}^z{\\!}p^g_{E2}')
xpgE3, ypgE3, zpgE3         = symbols('{\\\,}^x{\\!}p^g_{E3} {\\\,}^y{\\!}p^g_{E3} {\\\,}^z{\\!}p^g_{E3}')
xpgE4, ypgE4, zpgE4         = symbols('{\\\,}^x{\\!}p^g_{E4} {\\\,}^y{\\!}p^g_{E4} {\\\,}^z{\\!}p^g_{E4}')
pgE1                        = Matrix([xpgE1, ypgE1, zpgE1])
pgE2                        = Matrix([xpgE2, ypgE2, zpgE2])
pgE3                        = Matrix([xpgE3, ypgE3, zpgE3])
pgE4                        = Matrix([xpgE4, ypgE4, zpgE4])
om1s,om2s,om3s,om4s         = symbols('\o{\omega}_1 \o{\omega}_2 \o{\omega}_3 \o{\omega}_4')
z_ax                        = Matrix([0,0,1])


#----------------------------------------------------------------------------------------
#||||||||||||||||      MATRIX    FUNCTIONS           ||||||||||||||||||||||||||||||||||||
#----------------------------------------------------------------------------------------

#definicja funkcyjnej macierzy S(w)
S = Sk = lambda v: Matrix([[0, -v[2], v[1]],[v[2], 0, -v[0]],[-v[1], v[0], 0]])

#definicja funkcyjnych macierzy rotacji
def Rx(a):
    return Matrix([[1,0,0],
                  [0,cos(a),-sin(a)],
                  [0,sin(a),cos(a)]])

def Ry(a):
    return Matrix([[cos(a),0,sin(a)],
                  [0,1,0],
                  [-sin(a),0,cos(a)]])

def Rz(a):
    return Matrix([[cos(a),-sin(a),0],
                  [sin(a),cos(a),0],
                  [0,0,1]])

def Jac(R,dChi=dChi):
    dR = (diff(R,t))
    Sw = (R.T @ dR)
    omsw = Matrix([[Sw[(2,1)]],[Sw[(0,2)]],[Sw[(1,0)]]])
    iJ = simplify(omsw.jacobian(dChi))
    J = simplify(iJ.inverse())
    return J

def plot_results(sim,ctrl,traj):
    control = np.array([ctrl.log[ctrl.t.index(time)] for time in sim.t]).T
    trajectory = np.array([traj.log[traj.t.index(time)] for time in sim.t]).T
  
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(3,2,1)
    ax1.plot(sim.t,sim.y[0],label='x')
    ax1.plot(sim.t,sim.y[1],label='y')
    ax1.plot(sim.t,sim.y[2],label='z')
    ax1.legend(loc='best')
    ax1 = fig1.add_subplot(3,2,2)
    ax1.plot(sim.t,sim.y[3],label='$\phi$')
    ax1.plot(sim.t,sim.y[4],label='$\\theta$')
    ax1.plot(sim.t,sim.y[5],label='$\psi$')
    ax1.legend(loc='best')
    ax1 = fig1.add_subplot(3,2,3)
    ax1.plot(sim.t,sim.y[6],label='$u$')
    ax1.plot(sim.t,sim.y[7],label='$v$')
    ax1.plot(sim.t,sim.y[8],label='$w$')
    ax1.legend(loc='best')
    ax1 = fig1.add_subplot(3,2,4)
    ax1.plot(sim.t,sim.y[9],label='$p$')
    ax1.plot(sim.t,sim.y[10],label='$q$')
    ax1.plot(sim.t,sim.y[11],label='$r$')
    ax1.legend(loc='best')
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(1,1,1)
    ax2.plot(sim.t,control[0],label='$\omega_1$')
    ax2.plot(sim.t,control[1],label='$\omega_2$')
    ax2.plot(sim.t,control[2],label='$\omega_3$')
    ax2.plot(sim.t,control[3],label='$\omega_4$')
    ax2.legend(loc='best')
#     ax2.set_ylim([244,246])
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(1,1,1)
    ax3.plot(sim.t,sim.y[0]-trajectory[0],label='$x-x_d$')
    ax3.plot(sim.t,sim.y[1]-trajectory[1],label='$y-y_d$')
    ax3.plot(sim.t,sim.y[2]-trajectory[2],label='$z-z_d$')
    ax3.plot(sim.t,sim.y[3]-trajectory[3],label='$\phi-\phi_d$')
    ax3.plot(sim.t,sim.y[4]-trajectory[4],label='$\\theta-\\theta_d$')
    ax3.plot(sim.t,sim.y[5]-trajectory[5],label='$\psi-\psi_d$')
    ax3.legend(loc='best')
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(1,1,1)
    ax4.plot(sim.y[0],sim.y[1])
    ax4.plot(trajectory[0],trajectory[1])
    ax4.set_xlabel('x')
    ax4.set_ylabel('y')
    ax4.set_aspect('equal')


#----------------------------------------------------------------------------------------
#||||||||||||||||      CALCULATE      MATRICES        |||||||||||||||||||||||||||||||||||
#----------------------------------------------------------------------------------------

pba = Matrix([x,y,z])
Rba = Rz(psi) @ Ry(tta) @ Rx(phi)
Rab = Rba.T
Jba = Jac(Rba)
Jab = simplify(Jba.inverse())
Eta = Rba.col_join(zeros(3)).row_join(zeros(3).col_join(Jba))
iEta = simplify(Eta.inverse())

M = (m*eye(3)).row_join(-m * Sk(pag)).col_join((m * Sk(pag)).row_join(I)) # mass matrix
iM = simplify(M.subs({xpag:0,ypag:0,zpag:0}).inv()) # inverse mass matrix
C = (m*Sk(omaa)).row_join(-m*Sk(omaa)*Sk(pag)).col_join((m*Sk(pag)*Sk(omaa)).row_join(-Sk(omaa)*I))

# forces and torques

# -- gravity
Fbg = Matrix([0,0,-m*grav]) # force of gravity in frame b
Fag = Rab*Fbg               # force of gravity in frame a
Nag = Sk(pag)*Fag           # torque of gravity in frame a
Gag = Fag.col_join(Nag)     # vector of gravity forces and torques (R^6)

# -- bouyancy
Fbv = Matrix([0,0,rho*grav*Vol])
Fav = Rab*Fbv
Nav = Sk(pav)*Fav
Gav = Fav.col_join(Nav)

# -- restoring forces
Gar = Gag + Gav
Cad = diag(uCd,vCd,wCd,pCd,qCd,rCd)
Cal = Matrix([[0,0,0,0,0,0],[0,0,0,0,0,0],[uCl,vCl,0,pCl,qCl,rCl],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]])
Gaa = -Cad * mme(sgamaa,mme(gamaa,gamaa)) + Cal * mme(gamaa,gamaa)

# -- engines
NE1 = Matrix([0,0,-cN1*om1])
NE2 = Matrix([0,0, cN2*om2])
NE3 = Matrix([0,0,-cN3*om3])
NE4 = Matrix([0,0, cN4*om4])
FE1 = cF1*om1*z_ax
FE2 = cF2*om2*z_ax
FE3 = cF3*om3*z_ax
FE4 = cF4*om4*z_ax

F_tau = FE1 + FE2 + FE3 + FE4
N1_tau = Sk(pgE1)*FE1 + Sk(pgE2)*FE2 + Sk(pgE3)*FE3 + Sk(pgE4)*FE4
N_tau = NE1 + NE2 + NE3 + NE4 + N1_tau

tau_quad = F_tau.col_join(N_tau)

#----------------------------------------------------------------------------------------
#||||||||||      KINEMATICS      AND       DYNAMICS        ||||||||||||||||||||||||||||||
#----------------------------------------------------------------------------------------

# Kinematics
d_pba = Rba * nuaa
d_Chi = Jba * omaa

Kin = d_pba.col_join(d_Chi)

# Dynamics
Ga_ext = Gar + Gaa # sum of of external forces and torques
d_gamaa = iM * (tau_quad + Ga_ext - C*gamaa)

Dyn = d_gamaa

# differential state
diff_state = Kin.col_join(Dyn)


#----------------------------------------------------------------------------------------
#||||||||||          DEFINE          PARAMETERS        ||||||||||||||||||||||||||||||||||
#----------------------------------------------------------------------------------------

params = {xs:0,ys:0,zs:0,phis:0,ttas:0,psis:0,us:0,vs:0,ws:0,ps:0,qs:0,rs:0}
params.update({uCd:0.01,vCd:0.01,wCd:0.01,pCd:0.01,qCd:0.01,rCd:0.01})
params.update({uCl:0.0,vCl:0.0,pCl:0.0,qCl:0.0,rCl:0.0})
params.update({Vol:0,rho:0,grav:9.81,m:1,Ix:0.1,Iy:0.1,Iz:0.1})
params.update({xpag:0,ypag:0,zpag:0})
params.update({cF:0.01,cN:0.001})
params.update({cF1:0.01,cF2:0.01,cF3:0.01,cF4:0.01})
params.update({cN1:0.001,cN2:0.001,cN3:0.001,cN4:0.001})
arm_len = 0.2
params.update({xpgE1:arm_len,ypgE1:0,zpgE1:0})
params.update({xpgE2:0,ypgE2:arm_len,zpgE2:0})
params.update({xpgE3:-arm_len,ypgE3:0,zpgE3:0})
params.update({xpgE4:0,ypgE4:-arm_len,zpgE4:0})
B = diff_state.jacobian(Matrix([om1,om2,om3,om4]))




#----------------------------------------------------------------------------------------
#||||||||||          FUNCTIONS FOR NUMERICAL CALCULATIONS        ||||||||||||||||||||||||
#----------------------------------------------------------------------------------------
M_num = M.subs(params)
G_num = lambdify((phi,tta,u,v,w,p,q,r),(Gar+Gaa).subs(params))
C_num = lambdify((p,q,r),C.subs(params))
Eta_num = lambdify((phi,tta,psi),Eta)
iEta_num = lambdify((phi,tta,psi),iEta)
iB_num = MAT(B.subs(params)[8:12,0:4].inv())
Rab_num = lambdify((phi,tta,psi),Rab)
Rba_num = lambdify((phi,tta,psi),Rba)
Jba_num = lambdify((phi,tta,psi),Jba)
Jab_num = lambdify((phi,tta,psi),Jab)
F_num = lambdify((phi,tta,psi,u,v,w,p,q,r,om1,om2,om3,om4),diff_state.subs(params))

#----------------------------------------------------------------------------------------
#||||||||||                                            ||||||||||||||||||||||||||||||||||
#----------------------------------------------------------------------------------------
def limitation(a, limit):
    for i in range(a.shape[0]):
        if a[i]>limit:
            a[i]=limit
        if a[i]<-limit:
            a[i]=-limit
    return a

def sigmoid(a, delta):
    return a.dot(a)/(a.dot(a)+delta)

def sigmoid_vec(a, delta):
    for i in range(a.shape[0]):
        if a[i] > 0:
            a[i]/=np.absolute(a[i])+delta
        else:
            a[i]/=np.absolute(a[i])-delta
    return a

def quadrotor(t,state):
    tr = traj(t,state)
    engines = quad_controller(t,state,tr)
    x,y,z,phi,tta,psi,u,v,w,p,q,r = state
    om1,om2,om3,om4 = engines
    diff_state = np.ndarray.tolist(F_num(phi,tta,psi,u,v,w,p,q,r,om1,om2,om3,om4).T)[0]
    return diff_state

def calculate_forces(state, tr):
    x,y,z,phi,tta,psi,u,v,w,p,q,r = state
    xd,yd,zd,phid,ttad,psid,ud,vd,wd,pd,qd,rd = tr
    
    eta     = Matrix([x,y,z,phi,tta,psi])
    etad    = Matrix([xd,yd,zd,phid,ttad,psid])
    gamaa   = Matrix([u,v,w,p,q,r])
    gamaad  = Matrix([ud,vd,wd,pd,qd,rd])


def stabilize_in_position(state, tr):
    x,y,z,phi,tta,psi,u,v,w,p,q,r = state
    xd,yd,zd,phid,ttad,psid,ud,vd,wd,pd,qd,rd = tr
    
    eta     = Matrix([x,y,z,phi,tta,psi])
    etad    = Matrix([xd,yd,zd,phid,ttad,psid])
    d_etad  = Matrix([ud,vd,wd,pd,qd,rd])
    gamaa   = Matrix([u,v,w,p,q,r])
    gamaad  = Matrix([ud,vd,wd,pd,qd,rd])
    iEtan   = iEta_num(phi,tta,psi)
    Mn      = M_num
    Gn      = G_num(phi,tta,u,v,w,p,q,r)
    Cn      = C_num(p,q,r)
    
    K_kin = eye(6)/3
    K_dyn = eye(6)*3
    #K_dyn = 3*Matrix([[1,0,0,0,0,0],[0,1,0,0,0,0],[0,0,3,0,0,0],[0,0,0,1,0,0],[0,0,0,0,1,0],[0,0,0,0,0,1]])

    e1      = etad - eta
    #gam     = gamaad + iEtan*d_etad + iEtan*K_kin*e1
    gam     = iEtan*(K_kin*e1+gamaad)
    e2      = gam - gamaa
    tau     = Mn*K_dyn*e2 - Gn + Cn*gamaa
    return tau

def outer_loop(state,tr):
    x,y,z,phi,tta,psi,u,v,w,p,q,r = state
    xd,yd,zd,phid,ttad,psid,ud,vd,wd,pd,qd,rd = tr
    pba     = Matrix([x,y,z])
    pbd     = Matrix([xd,yd,zd])
    nuaa    = Matrix([u,v,w])
    Rban    = Rba_num(phi,tta,psi)
    d_pba   = Rban*nuaa
    d_pbd   = Matrix([ud,vd,wd])
    Rabn    = Rab_num(phi,tta,psi)
    Jban    = Jba_num(phi,tta,psi)

    kappa = 0.3
 
    e1 = pbd-pba
    #e2 = d_pbd-d_pba

    #ee = e1.dot(e2)
    #k1 = 1
    #k2 = 1
    K_kin = 1*eye(3)
    K_dyn = 1*eye(3)
    
    d_pbd = K_kin * e1
    e2 = K_dyn * (d_pbd - d_pba)
    
    eb = Sk(z_ax) * e2
    eb = sigmoid_vec(eb, 1)
    #eb/= (eb.dot(eb)+1)
    ea = Rabn*eb
    Chid = Jban * kappa * ea #* sigmoid(e1,1)


    #Chid = limitation(Jban * K_dyn*e3,kappa)


    #e = k1*e1 + k2*e2
    #ab = Sk(z_ax)*e
    #ab/= np.abs(ab.T*ab)
    #aa = Rabn*ab
    #dChid = Jban * kappa * aa * sigmoid(e,2)
    #Chid = dChid * 1
    #Chid = limitation(Chid,pi/12) 

    tra = tr
    tra[3:6] = Chid

    return tra

@log_results
def quad_controller(t,state,tr):
    x,y,z,phi,tta,psi,u,v,w,p,q,r = state
    xd,yd,zd,phid,ttad,psid,ud,vd,wd,pd,qd,rd = tr
    #Rban    = Rba_num(phi,tta,psi)
    #Rabn    = Rab_num(phi,tta,psi)
    #Jban    = Jba_num(phi,tta,psi)
    iBn     = iB_num
    #pba     = Matrix([x,y,z])
    #pbd     = Matrix([xd,yd,zd])
    #nuaa    = Matrix([u,v,w])
    #d_pba   = Rban*nuaa
    #d_pbd   = Matrix([ud,vd,wd])

    #q1       = pbd - pba
    #q2       = d_pbd - d_pba
    #q3        = q1 + q2 
    #q = Jban*Rabn*q3*sigmoid(q3,10)
    
    #tx = q[0]
    #ty = q[1]
    #tz = q[2]
    #phid   -= atan2(ty,tz)
    #ttad   += atan2(tx,tz)
    #phid    = atan2(ty,tz)/2
    #ttad    = atan2(tx,tz)/2
    #tra      = tr
    #tra[3:6] = q
    tra     = outer_loop(state, tr)
    tau     = stabilize_in_position(state,tra)
    #tau[2] /= (cos(phi)*cos(tta))
    
    engines = iBn@tau[2:6]
    #return engines
    return limitation(engines,300)


@log_results
def traj(t,state):
    #t = symbols('t')    
    xd = 0
    yd = 0
    zd = 0
    phid = 0
    ttad = 0
    psid = 0
    ud = 0
    vd = 0
    wd = 0
    pd = 0
    qd = 0
    rd = 0
    #ud = diff(xd,t)
    #vd = diff(yd,t)
    #wd = diff(zd,t)
    #pd = diff(phid,t)
    #qd = diff(ttad,t)
    #rd = diff(psid,t)
    
    #param = {t:time}
    #xd = xd.subs(param)
    #yd = yd.subs(param)
    #zd = zd.subs(param)
    #phid = phid.subs(param)
    #ttad = ttad.subs(param)
    #psid = psid.subs(param)
    #ud = ud.subs(param)
    #vd = vd.subs(param)
    #wd = wd.subs(param)
    #pd = pd.subs(param)
    #qd = qd.subs(param)
    #rd = rd.subs(param)
    
    traj = [xd,yd,zd,phid,ttad,psid,ud,vd,wd,pd,qd,rd]
    return traj


state0 = [5,5,0,0,0,0,0,0,0,0,0,0]
#state0 = [0.3,-0.4,1,0.1,-0.2,-0.3,0.2,0.2,0.2,1,2,3]
sim = solve_ivp(quadrotor,[0,200],state0,max_step=1)
plot_results(sim,quad_controller,traj)  
plot()

print("siemanko")
